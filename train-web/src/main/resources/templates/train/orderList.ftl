<link rel="stylesheet" type="text/css" href="${request.contextPath}/static/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="${request.contextPath}/static/css/bootstrap-theme.min.css" />
<script type="text/javascript">
$(function(){
	$("#choicePassenger").click(function(){
		var passengerTicketStr="";
		var oldPassengerStr="";
		var html="";
		var seatType=$("input[name='seatType']:checkbox:checked").val();
		$("input[name='passenger']:checkbox:checked").each(function(){
			passengerTicketStr+=seatType+",0,"+$(this).val()+","+$(this).attr("mobile_no")+",N_";
			oldPassengerStr+=$(this).val().substring(2)+",1_";
			var type=$("input[name='seatType']:checkbox:checked").attr("types")
			html+="<label for='avail_ticket' style='cursor: pointer;'>乘客名："+$(this).attr("passenger_name")+"   座位类型："+type+"</label>  ";
		});
		passengerTicketStr=passengerTicketStr.substring(0,passengerTicketStr.length-1);
		$("#passengerTicketStr").val(passengerTicketStr);
		$("#oldPassengerStr").val(oldPassengerStr);
		$('#myModal').modal('hide');
		$(".fr").html(html);
	})
});
</script>
<table width="80%" style="margin: 0 auto;" class="table table-bordered" id="passenger">
	<tr align="center">
		<td>订单号 <td>
		<td>订单时间<td>
		<td>发车时间<td>
		<td>车次   <td>
		<td>出发地 <td>
		<td>目的地 <td>
		<td>乘客   <td>
		<td>票种   <td>
		<td>席别   <td>
		<td>车厢   <td>
		<td>座位   <td>
		<td>票价   <td>
		<td>状态   <td>
	</tr>
	 <#list list as passenger>
		<tr align="center">
			<td><input type="checkbox" name="passenger" passenger_name="${passenger.passenger_name}" value="${passenger.passenger_type},${passenger.passenger_name},${passenger.passenger_id_type_code},${passenger.passenger_id_no}" mobile_no="${passenger.mobile_no}"/>${passenger_index+1}</td>
			<td>${passenger.passenger_name }</td>
			<td>${passenger.passenger_id_type_name}</td>
			<td>${passenger.passenger_id_no }</td>
			<td>${passenger.mobile_no }${passenger.phone_no }</td>
			<td>${passenger.passenger_type_name}</td>
			<td>
				<#if passenger.passenger_flag=='0'>已通过</#if>
				<#if passenger.passenger_flag!='0'>未通过</#if>
			</td>
		</tr>
	 </#list>
	</#if>
</table>
